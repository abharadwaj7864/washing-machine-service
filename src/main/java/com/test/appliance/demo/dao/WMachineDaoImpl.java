package com.test.appliance.demo.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.test.appliance.demo.model.WashingMachine;

@Repository
public class WMachineDaoImpl implements WMachineDao {

	@Autowired
	private WMachineRep repo;

	@Override
	public WashingMachine start(WashingMachine machine) {
		return repo.save(machine);
		
	}

	@Override
	public WashingMachine stop(WashingMachine machine) {
		return repo.save(machine);
		
	}

	@Override
	public String state(Long id) {
		WashingMachine resp = repo.findMachineById(id);
		return resp.getState();
		
	}

	@Override
	public WashingMachine spin(WashingMachine machine) {
		return repo.save(machine);
		
	}

	@Override
	public WashingMachine dry(WashingMachine machine) {
		return repo.save(machine);
		
	}
	
	@Override
    public WashingMachine saveMachine(WashingMachine machine){
    	return repo.save(machine);
    }
	
	@Override
	public WashingMachine updateMachine(WashingMachine machine){
		return repo.save(machine);
		
	}
	
	@Override
	public List<WashingMachine> getMachines(){
		return repo.findAll();
	}

}
