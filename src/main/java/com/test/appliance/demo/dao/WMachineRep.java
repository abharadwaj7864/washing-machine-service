package com.test.appliance.demo.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.test.appliance.demo.model.WashingMachine;

public interface WMachineRep extends CrudRepository<WashingMachine, Long> {

	WashingMachine save(WashingMachine washing);

	WashingMachine findMachineById(Long Id);
	
	List<WashingMachine> findAll();

}
