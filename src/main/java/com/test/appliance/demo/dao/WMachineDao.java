package com.test.appliance.demo.dao;

import java.util.List;

import com.test.appliance.demo.model.WashingMachine;

public interface WMachineDao {
	
    WashingMachine saveMachine(WashingMachine machine);
	
	WashingMachine updateMachine(WashingMachine machine);
	
	List<WashingMachine> getMachines();

	WashingMachine start(WashingMachine machine);

	WashingMachine stop(WashingMachine machine);

	WashingMachine spin(WashingMachine machine);

	WashingMachine dry(WashingMachine machine);

	String state(Long id);

}
