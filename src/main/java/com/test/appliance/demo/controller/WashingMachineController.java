package com.test.appliance.demo.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.test.appliance.demo.model.WMachineResponse;
import com.test.appliance.demo.model.WashingMachine;
import com.test.appliance.demo.service.WMachineService;
import com.test.appliance.demo.util.WMachineState;

@RestController
public class WashingMachineController {

	private final Logger LOGGER = LoggerFactory.getLogger(WashingMachineController.class);

	@Autowired
	private WMachineService service;

	@RequestMapping(value = "/saveMachine", method = RequestMethod.POST)
	public ResponseEntity<?> saveMachine(@RequestBody WashingMachine machine) throws InterruptedException {
		WashingMachine resp = null;
		try {
			resp = service.saveMachine(machine);

		} catch (Exception e) {
			return new ResponseEntity<WashingMachine>(resp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<WashingMachine>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/updateMachine", method = RequestMethod.PUT)
	public ResponseEntity<?> updateMachine(@RequestBody WashingMachine machine) throws InterruptedException {
		WashingMachine resp = null;
		try {
			resp = service.updateMachine(machine);

		} catch (Exception e) {
			return new ResponseEntity<WashingMachine>(resp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<WashingMachine>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/getMachines", method = RequestMethod.GET)
	public ResponseEntity<?> getMachines() throws InterruptedException {
		List<WashingMachine> resp = null;
		try {
			resp = service.getMachines();

		} catch (Exception e) {
			return new ResponseEntity<List<WashingMachine>>(resp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<WashingMachine>>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/start", method = RequestMethod.POST)
	public ResponseEntity<?> start(@RequestBody WashingMachine machine) throws InterruptedException {
		WMachineResponse response = null;
		try {
			WashingMachine resp = service.start(machine);
			response = new WMachineResponse();
			response.setMachine(resp);
			response.setStatus(WMachineState.STARTED.toString());
		} catch (Exception e) {
			return new ResponseEntity<WMachineResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<WMachineResponse>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/state/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getState(@PathVariable Long id) {
		String resp = null;
		try {
			LOGGER.info("State request");
			resp = service.state(id);
			return new ResponseEntity<String>(resp, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>("Fetching State Failed!", HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@RequestMapping(value = "/stop", method = RequestMethod.POST)
	public ResponseEntity<?> stop(@RequestBody WashingMachine machine) {
		WMachineResponse response = null;
		try {
			WashingMachine resp = service.stop(machine);
			response = new WMachineResponse();
			response.setMachine(resp);
			response.setStatus(WMachineState.STOPPED.toString());
			return new ResponseEntity<WMachineResponse>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<WMachineResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@RequestMapping(value = "/spin", method = RequestMethod.POST)
	public ResponseEntity<?> spin(@RequestBody WashingMachine machine) {
		WMachineResponse response = null;
		try {
			WashingMachine resp = service.spin(machine);
			response = new WMachineResponse();
			response.setMachine(resp);
			response.setStatus(WMachineState.SPINNING.toString());
			return new ResponseEntity<WMachineResponse>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<WMachineResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@RequestMapping(value = "/dry", method = RequestMethod.POST)
	public ResponseEntity<?> dry(@RequestBody WashingMachine machine) {
		WMachineResponse response = null;
		try {
			WashingMachine resp = service.dry(machine);
			response = new WMachineResponse();
			response.setMachine(resp);
			response.setStatus(WMachineState.DRYING.toString());
			return new ResponseEntity<WMachineResponse>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<WMachineResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

}
