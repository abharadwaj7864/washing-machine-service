package com.test.appliance.demo.model;

public class WMachineResponse {

	private WashingMachine machine;
	private String status;

	public WashingMachine getMachine() {
		return machine;
	}

	public void setMachine(WashingMachine machine) {
		this.machine = machine;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
