package com.test.appliance.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.appliance.demo.dao.WMachineDao;
import com.test.appliance.demo.model.WashingMachine;

@Service
public class WMachineServiceImpl implements WMachineService {

	@Autowired
	private WMachineDao dao;
	
     public WashingMachine saveMachine(WashingMachine machine){
    	 return dao.saveMachine(machine);
     }
	
	 public WashingMachine updateMachine(WashingMachine machine){
		 return dao.updateMachine(machine);
	 }
	
	 public List<WashingMachine> getMachines(){
		 return dao.getMachines();
		 
	 }

	@Override
	public WashingMachine start(WashingMachine machine) {

		return dao.start(machine);

	}

	@Override
	public WashingMachine stop(WashingMachine machine) {
		return dao.stop(machine);
	}

	@Override
	public WashingMachine spin(WashingMachine machine) {
		return dao.spin(machine);
	}

	@Override
	public WashingMachine dry(WashingMachine machine) {
		return dao.dry(machine);
	}

	@Override
	public String state(Long id) {
		return dao.state(id);
	}

}
