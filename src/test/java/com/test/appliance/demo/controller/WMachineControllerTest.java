package com.test.appliance.demo.controller;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import com.test.appliance.demo.model.WashingMachine;
import com.test.appliance.demo.service.WMachineService;
import com.test.appliance.demo.service.WMachineServiceImpl;

import junit.framework.Assert;

public class WMachineControllerTest {
	
	@InjectMocks
	WashingMachineController controller;
	
	@Mock
	WMachineService service;
	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testGetMachines() throws InterruptedException {
		List<WashingMachine> list = new ArrayList<>();
		WashingMachine machine = new WashingMachine();
		machine.setModelName("test");
		machine.setSerialId("t1");
		machine.setState("START");
		list.add(machine);
		when(service.getMachines()).thenReturn(list);

		ResponseEntity<List<WashingMachine>> resp = (ResponseEntity<List<WashingMachine>>) controller.getMachines();
		Assert.assertEquals("START", resp.getBody().get(0).getState());
		verify(service, Mockito.times(1)).getMachines();

	}
	
	@Test
	public void testsaveMachine() throws InterruptedException {
		//List<WashingMachine> list = new ArrayList<>();
		WashingMachine machine = new WashingMachine();
		machine.setModelName("test");
		machine.setSerialId("t1");
		machine.setState("START");
		//list.add(machine);
		when(service.saveMachine(machine)).thenReturn(machine);

		ResponseEntity<WashingMachine> resp =  (ResponseEntity<WashingMachine>) controller.saveMachine(machine);
		Assert.assertEquals("START", resp.getBody().getState());
		verify(service, Mockito.times(1)).saveMachine(machine);

	}
	
	@Test
	public void testUpdateMachine() throws InterruptedException {
		//List<WashingMachine> list = new ArrayList<>();
		WashingMachine machine = new WashingMachine();
		machine.setModelName("test2");
		machine.setSerialId("t2");
		machine.setState("START");
		//list.add(machine);
		when(service.updateMachine(machine)).thenReturn(machine);

		ResponseEntity<WashingMachine> resp =  (ResponseEntity<WashingMachine>) controller.updateMachine(machine);
		Assert.assertEquals("START", resp.getBody().getState());
		verify(service, Mockito.times(1)).updateMachine(machine);

	}
	
	/*@Test
	public void testStart() throws InterruptedException {
		//List<WashingMachine> list = new ArrayList<>();
		WashingMachine machine = new WashingMachine();
		machine.setModelName("test");
		machine.setSerialId("t1");
		machine.setState("START");
		//list.add(machine);
		when(service.start(machine)).thenReturn(machine);

		ResponseEntity<WashingMachine> resp =  (ResponseEntity<WashingMachine>) controller.start(machine);
		Assert.assertEquals("START", resp.getBody().getState());
		verify(service, Mockito.times(1)).start(machine);

	}
*/



}
