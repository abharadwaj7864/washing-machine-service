package com.test.appliance.demo.service;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.test.appliance.demo.dao.WMachineDaoImpl;
import com.test.appliance.demo.model.WashingMachine;

import junit.framework.Assert;

public class WMachineServiceTest {

	@InjectMocks
	WMachineServiceImpl service;

	@Mock
	WMachineDaoImpl dao;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testStart() {

		WashingMachine machine = new WashingMachine();
		machine.setModelName("test");
		machine.setSerialId("t1");
		machine.setState("STARTED");
		when(dao.start(ArgumentMatchers.any())).thenReturn(machine);

		WashingMachine resp = service.start(machine);
		//Assert.assertEquals("STARTED", resp.getState());
		verify(dao, Mockito.times(1)).start(ArgumentMatchers.any());

	}
	
	@Test
	public void testStop() {

		WashingMachine machine = new WashingMachine();
		machine.setModelName("test");
		machine.setSerialId("t1");
		machine.setState("STOPPED");
		when(dao.stop(ArgumentMatchers.any())).thenReturn(machine);

		WashingMachine resp = service.stop(machine);
		Assert.assertEquals("STOPPED", resp.getState());
		verify(dao, Mockito.times(1)).stop(ArgumentMatchers.any());

	}

	@Test
	public void testSpin() {

		WashingMachine machine = new WashingMachine();
		machine.setModelName("test");
		machine.setSerialId("t1");
		machine.setState("SPINNING");
		when(dao.spin(ArgumentMatchers.any())).thenReturn(machine);

		WashingMachine resp = service.spin(machine);
		Assert.assertEquals("SPINNING", resp.getState());
		verify(dao, Mockito.times(1)).spin(ArgumentMatchers.any());

	}
	
	@Test
	public void testDry() {

		WashingMachine machine = new WashingMachine();
		machine.setModelName("test");
		machine.setSerialId("t1");
		machine.setState("DRYING");
		when(dao.dry(ArgumentMatchers.any())).thenReturn(machine);

		WashingMachine resp = service.dry(machine);
		Assert.assertEquals("DRYING", resp.getState());
		verify(dao, Mockito.times(1)).dry(ArgumentMatchers.any());

	}


	
	@Test
	public void testGetMachines() {
		List<WashingMachine> list = new ArrayList<>();
		WashingMachine machine = new WashingMachine();
		machine.setModelName("test");
		machine.setSerialId("t1");
		machine.setState("START");
		list.add(machine);
		when(dao.getMachines()).thenReturn(list);

		List<WashingMachine> list1 = service.getMachines();
		Assert.assertEquals("START", list1.get(0).getState());
		verify(dao, Mockito.times(1)).getMachines();

	}
	
	@Test
	public void saveMachine() {
		//List<WashingMachine> list = new ArrayList<>();
		WashingMachine machine = new WashingMachine();
		machine.setModelName("test");
		machine.setSerialId("t1");
		machine.setState("START");
		
		when(dao.saveMachine(machine)).thenReturn(machine);

		WashingMachine resp = service.saveMachine(machine);
		Assert.assertEquals("START", resp.getState());
		verify(dao, Mockito.times(1)).saveMachine(machine);

	}

	@Test
	public void updateMachine() {
		//List<WashingMachine> list = new ArrayList<>();
		WashingMachine machine1 = new WashingMachine();
		machine1.setModelName("test");
		machine1.setSerialId("t1");
		machine1.setState("START");
		
		when(dao.updateMachine(machine1)).thenReturn(machine1);

		WashingMachine resp = service.updateMachine(machine1);
		Assert.assertEquals("START", resp.getState());
		verify(dao, Mockito.times(1)).updateMachine(machine1);

	}


	
	/*@Test
	public void testStop() {

		when(dao.stop(ArgumentMatchers.any())).thenReturn("STOPPED");

		String resp = service.stop();
		Assert.assertEquals("STOPPED", resp);
		verify(dao, Mockito.times(1)).stop(ArgumentMatchers.any());

	}

	@Test
	public void testGetState() {

		when(dao.state()).thenReturn("STARTED");

		String resp = service.state();
		Assert.assertEquals("STARTED", resp);
		verify(dao, Mockito.times(1)).state();

	}*/

}
