#Backend Service 

### Washing Machine Controller Api Design

The task is to design and implement a backend service to control an appliance such as a wash machine or an oven. The API should be REST based and the state of the appliance should be persisted to any form of persistent storage. There is no need for any frontend but we expect there to be a README.md file with build directions and examples of how to invoke the REST API (e.g. curl).

Bitbucket has been used to publish the source code.


###Installation and Run

$ mvn spring-boot:run

###REST API description and test

http://localhost:8080/swagger-ui.html#/

GET /getMachines - View all machines

GET /state/{id} - Shows state of a machine by its id

POST /saveMachine - Persists the machines

POST /start - Saves machines state as START

POST /stop - Saves machines state as STOP

POST /dry -	Saves machines state as DRYING

POST /spin - Saves machines state as SPINNING



